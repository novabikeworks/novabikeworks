angular.module('myApp').run(function($httpBackend, $filter) {

	$httpBackend.whenRoute('GET', '/rest/categories')
	  .respond(function(method, url, data, headers, params) {
		  var selectedItems =  _.get(categories, {category: params.category});
	    return [200, categories];
	  });
	
	$httpBackend.whenRoute('GET', '/rest/items')
	  .respond(function(method, url, data, headers, params) {
		  var selectedItems =  _.get(items, {category: params.category});
	    return [200, selectedItems];
	  });

	$httpBackend.whenGET('/rest/items/:id')
	.respond(function(method, url, data, headers, params) {
	  return [200, $filter(items, {id: params.id})];
	});

	$httpBackend.whenPUT('/rest/items/:id')
	.respond(function(method, url, data, headers, params) {
		items[params.id-1]= data;
		return [200, data];
	});

	$httpBackend.whenPOST('/rest/items/')
	.respond(function(method, url, data, headers, params) {
		var item = data;
		item.id = items.length + 1;
		items.push(item);
		return [200, item];
	});

	$httpBackend.whenGET(/.*/).passThrough(); 
	$httpBackend.whenJSONP(/.*/).passThrough(); 
	
	$httpBackend.whenPOST(/*temprotect*/).passThrough(); 

	var categories=[{id:1, category: 'road', title: 'Road Bike', brand: 'Mekk', model: 'Poggio SE 0.3', sizes:[50,52,54,56,58], bullets:['Full Carbon Frame', 'Shimano Tiagra Components','Classic Road Bike Geometry'], price: 35.00, image:'poggio.jpg'},
	                       {id:2, category: 'road', title: 'Road Bike',  brand: 'Mekk', model: 'Poggio 3.5', sizes:[50, 53, 54.5, 56, 59],bullets:['High Modulus Carbon Toray T800', 'Di2 Ultegra Components','Frame and fork re-design '], price: 45.00, image:'poggio3.5.jpg' },
	                       {id:3, category: 'road', title: 'Road Bike',  brand: 'Mekk', model: 'Primo 6.0', sizes:[50, 52, 54, 58],bullets:['Aero-optimized monocoque carbon frame', 'Shimano Ultegra 6800 mechanical','Deep section carbon wheelset'], price: 65.00, image:'mekk.jpg'}];

	var items = [{id: 1, brand: 'Mekk', model: 'Poggio SE 0.3', size: 50, category: 'road', rented:[],bullets:[], image:'poggio.jpg'},
	{id: 1, brand: 'Mekk', model: 'Poggio SE 0.3', size: 52, category: 'road', rented:[], bullets:[], image:'poggio.jpg'},
	{id: 2, brand: 'Mekk', model: 'Poggio SE 0.3', size: 54, category: 'road', rented:[], bullets:[], image:'poggio.jpg'},
	{id: 3, brand: 'Mekk', model: 'Poggio SE 0.3', size: 56, category: 'road', rented:[], bullets:[], image:'poggio.jpg'},
	{id: 4, brand: 'Mekk', model: 'Poggio SE 0.3', size: 58, category: 'road', rented:[], bullets:[], image:'poggio.jpg'},
	{id: 5, brand: 'Mekk', model: 'Poggio 3.5', size: 50, category: 'road', rented:[], bullets:[], image:'602.jpg'},
	{id: 6, brand: 'Mekk', model: 'Poggio 3.5', size: 54.5, category: 'road', rented:[], bullets:[], image:'poggio.jpg'},
	{id: 7, brand: 'Mekk', model: 'Poggio 3.5', size: 56, category: 'road', rented:[], bullets:[], image:'poggio.jpg'},
	{id: 8, brand: 'Mekk', model: 'Poggio 3.5', size: 58, category: 'road', rented:[], bullets:[], image:'poggio.jpg'},
	];
		 
		 
		});






