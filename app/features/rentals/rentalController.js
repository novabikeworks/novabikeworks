(function() {
	angular.module('myApp').controller('rentalController', rentalController);
	rentalController.$inject = [ '$routeParams', '$location', 'inventoryService', 'contactService'];
	function rentalController($routeParams, $location, inventoryService, contactService) {
		var vm = this;

		vm.category = $routeParams.category;
		vm.selectedModel = null
		vm.selectModel=selectModel;
		vm.email = {};
		vm.rent=rent;
		
		function rent(){
			vm.email.subject="Bike Rental";
			vm.email.message="Rental Request: Name: " + vm.email.name + "\nEmail: " + vm.email.address + "\nModel:" + vm.selectedModel.brand + " " + vm.selectedModel.model;
			vm.email.message += "\nSize: " + vm.selectedModel.size + "\nStart: " + vm.selectedModel.start.toDateString() + " \nEnd: " + vm.selectedModel.end.toDateString();
			contactService.sendEmail(vm.email);
			vm.selectedModel=null;
			$location.path('/thanks');
			
		}
		
		function selectModel(item, size){
			
			vm.categories.forEach(function(entry){
				entry.size=null
			});
			vm.selectedModel = item;
			vm.selectedModel.size=size;
		}
		
		inventoryService.getCategories({category: vm.category}).then(function(response){
			vm.categories=response.data;
			
		});
		
		vm.today = function() {
			vm.dt = new Date();
		};
		vm.today();

		vm.clear = function() {
			vm.dt = null;
		};

		vm.inlineOptions = {
			customClass : getDayClass,
			minDate : new Date(),
			showWeeks : true
		};

		vm.dateOptions = {
			
			formatYear : 'yy',
			maxDate : new Date(2020, 5, 22),
			minDate : new Date(),
			startingDay : 1
		};

		// Disable weekend selection
		function disabled(data) {
			var date = data.date,
				mode = data.mode;
			return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
		}

		vm.toggleMin = function() {
			vm.inlineOptions.minDate = vm.inlineOptions.minDate ? null : new Date();
			vm.dateOptions.minDate = vm.inlineOptions.minDate;
		};

		vm.toggleMin();

		vm.openStart = function() {
			vm.popup1.opened = true;
		};

		vm.openEnd = function() {
			vm.popup2.opened = true;
		};

		vm.setDate = function(year, month, day) {
			vm.dt = new Date(year, month, day);
		};

		vm.formats = [ 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate' ];
		vm.format = vm.formats[0];
		vm.altInputFormats = [ 'M!/d!/yyyy' ];

		vm.popup1 = {
			opened : false
		};

		vm.popup2 = {
			opened : false
		};

		var tomorrow = new Date();
		tomorrow.setDate(tomorrow.getDate() + 1);
		var afterTomorrow = new Date();
		afterTomorrow.setDate(tomorrow.getDate() + 1);
		vm.events = [
			{
				date : tomorrow,
				status : 'full'
			},
			{
				date : afterTomorrow,
				status : 'partially'
			}
		];

		function getDayClass(data) {
			var date = data.date,
				mode = data.mode;
			if (mode === 'day') {
				var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

				for (var i = 0; i < vm.events.length; i++) {
					var currentDay = new Date(vm.events[i].date).setHours(0, 0, 0, 0);

					if (dayToCheck === currentDay) {
						return vm.events[i].status;
					}
				}
			}

			return '';
		}

	}

})();