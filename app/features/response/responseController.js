(function() {
	angular.module('myApp').controller('responseController', responseController);
	responseController.$inject = [ 'contactService' ];
	
	function responseController(contactService) {
		var vm = this;
		vm.email = contactService.getLastEmail();

	}

})();