(function() {
	angular.module('myApp').service('inventoryService', inventoryService);
	inventoryService.$inject = [ '$http' ];
	function inventoryService($http) {
		var vm = this;
		vm.getCategories = getCategories;

		function getCategories(query) {
			var promise = $http({
				url: '/rest/categories',
				params: query,
				method: 'GET'
			}).then(function(response) {
				return response;
			}).catch(function(e){
			   console.log(e);
			});
			return promise;
		}
	}
})();