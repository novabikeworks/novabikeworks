
	angular.module('myApp').controller('fitController', fitController);
	fitController.$inject = ['contactService'];
	function fitController(contactService) {
	var vm=this;
	vm.showEmail = false;
	vm.schedule=schedule;
	vm.email={};
	
	function schedule(){
		
		vm.email.subject="Bike Fitting Request Date";
		vm.email.message="Date Requested: " + vm.dt.toDateString();
		contactService.sendEmail(vm.email);
		vm.selectedModel=null;
		
	}
	
	
	vm.today = function() {
		vm.dt = new Date();
  };
  vm.today();

  vm.clear = function () {
	  vm.dt = null;
  };

  
  vm.toggleMin = function() {
	  vm.minDate = vm.minDate ? null : new Date();
  };
  vm.toggleMin();

  vm.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened = true;
  };

  vm.dateOptions = {
		    dateDisabled: disabled,
		    formatYear: 'yy',
		    maxDate: new Date(2020, 5, 22),
		    minDate: new Date(),
		    startingDay: 1
		  };

  // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
  }

  vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  vm.format = vm.formats[0];
}