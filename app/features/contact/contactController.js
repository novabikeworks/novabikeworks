(function() {
	angular.module('myApp').controller('contactController', contactController);
	contactController.$inject = [ '$routeParams', 'contactService', '$location' ];
	function contactController($routeParams, contactService, $location) {
		var vm = this;
		vm.email = $routeParams.email;
		vm.sendEmail = sendEmail;

		if (vm.email) {
			sendEmail(vm.email);
		}

		function sendEmail(email) {
			contactService.sendEmail(email);
		}

	}

})();