(function() {
	angular.module('myApp').service('contactService', contactService);
	contactService.$inject = [ '$http', '$sce', '$location'];
	function contactService($http, $sce, $location) {
		var vm = this;
		vm.email;
		vm.sendEmail = sendEmail;
		vm.getLastEmail = getLastEmail;
		
		function getLastEmail(){
			return vm.email;
		}
		
		function sendEmail(email) {
			vm.email=email;
			var dataObject = {
				sendMail : "Send",
				email : email,
			};
			var queryString = 'email.name=' + encodeURIComponent(email.name) + '&email.from=' + encodeURIComponent(email.address) + '&email.subject=' +
			encodeURIComponent(email.subject) + '&email.message=' + encodeURIComponent(email.message) + '&sendMail=Send';
			$sce.trustAsHtml(url);
			var url = 'https://www.temprotect.com/action/mail?' + queryString
			$http.jsonp(url, {jsonpCallbackParam: 'callback'});
			$location.path('/thanks');
		};
	}

})();