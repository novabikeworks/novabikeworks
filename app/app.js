angular.module('myApp', ['ngRoute', 'ui.bootstrap', 'ngMockE2E']);
angular.module('myApp').config(function($routeProvider, $locationProvider) {
	
    $routeProvider
    .when("/", {
        templateUrl : "app/partials/home.html"
    })
    .when("/rent/:category", {
        templateUrl : "app/features/rentals/rental.html"
    })
    .when("/contact", {
        templateUrl : "app/features/contact/contact.html",
        controller : 'contactController',
        controllerAs: 'vm'
    })
    .when("/about", {
        templateUrl : "app/partials/about.html"
    })
     .when("/thanks", {
        templateUrl : "app/features/response/response.html",
        controller : 'responseController',
        controllerAs: 'vm'
    })
    .when("/fit", {
        templateUrl : "app/features/fit/fitting.html"
    });
});